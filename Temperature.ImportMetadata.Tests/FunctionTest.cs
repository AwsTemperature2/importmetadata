using Xunit;
using Amazon.Lambda.TestUtilities;
using Temperature.ImportMetadata.Domain;

namespace Temperature.ImportMetadata.Tests
{
    public class FunctionTest
    {
        public FunctionTest()
        {
        }

        [Fact]
        public void TetGetMethod()
        {
            var context = new TestLambdaContext();
            var request = new ImportFileData
            {

            };

            Functions functions = new Functions();
            
            var response = functions.Save(request, context);
            Assert.NotNull(response);
        }
    }
}
