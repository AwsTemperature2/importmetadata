using Xunit;
using Amazon.Lambda.TestUtilities;
using Temperature.ImportMetadata.Domain;
using System;
using System.Linq;

namespace Temperature.ImportMetadata.IntegrationTests
{
    public class FunctionIntegrationTest
    {
        public FunctionIntegrationTest()
        {
            Environment.SetEnvironmentVariable(Functions.EnvironmentVariable, "Development");
        }

        [Fact]
        public void TetGetMethod()
        {
            var context = new TestLambdaContext();
            var request = new ImportFileData
            {
                Bucket = "temperaturemm-upload",
                Key = "dev/jsoncmd-2018-09-01_00-00-01",
                Bytes = 82500,
                Records = 1440,
                BadRecords = 0,
                ProcessingStarted = DateTime.Parse("2018-10-13T19:45:43.2590682+00:00"),
                FirstRecordTimestamp = DateTime.Parse("2018-08-31T00:00:03"),
                LastRecordTimestamp = DateTime.Parse("2018-08-31T23:59:03"),
                Dates = new[] { "2018-08-31" }.ToList()
            };

            Functions functions = new Functions();

            var response = functions.Save(request, context);
            Assert.NotNull(response);
        }
    }
}
