using System.Threading.Tasks;
using Temperature.ImportMetadata.Domain;

namespace Temperature.ImportMetadata
{
    public interface IMetadataService
    {
        Task SaveMetadata(ImportFileData data);
    }
}
