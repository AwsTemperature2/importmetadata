using AutoMapper;
using System;
using System.Threading.Tasks;
using Temperature.ImportMetadata.Domain;
using Temperature.ImportMetadata.Infrastructure.DB;

namespace Temperature.ImportMetadata.Services
{
    public class MetadataService : IMetadataService
    {
        private readonly IRepository<ImportFileDataEntity> dataRepository;
        private readonly IMapper mapper;

        public MetadataService(IRepository<ImportFileDataEntity> dataRepository, IMapper mapper)
        {
            this.dataRepository = dataRepository ?? throw new ArgumentNullException(nameof(dataRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task SaveMetadata(ImportFileData data)
        {
            var entity = mapper.Map<ImportFileDataEntity>(data);
            await dataRepository.Save(entity);
        }
    }
}
