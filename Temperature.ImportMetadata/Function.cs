using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Microsoft.Extensions.DependencyInjection;
using Temperature.ImportMetadata.Domain;
using Temperature.ImportMetadata.Infrastructure.DB;
using Temperature.ImportMetadata.Infrastructure.Options;
using Temperature.ImportMetadata.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Temperature.ImportMetadata
{
    public class Functions : LambdaBase
    {
        public Functions()
        {

        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddAWSService<IAmazonDynamoDB>();
            services.AddTransient<IRepository<ImportFileDataEntity>, Repository<ImportFileDataEntity>>();
            services.AddTransient<IMetadataService, MetadataService>();
        }

        protected override void InjectOptions(IServiceCollection services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("ServiceOptions"));
            services.Configure<DynamoDbOptions>(Configuration.GetSection("DynamoDbOptions"));
        }

        public async Task Save(ImportFileData request, ILambdaContext context)
        {
            var service = this.GetService<IMetadataService>();
            await service.SaveMetadata(request);
        }
    }
}
