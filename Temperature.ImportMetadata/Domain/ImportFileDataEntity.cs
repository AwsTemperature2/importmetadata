﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;

namespace Temperature.ImportMetadata.Domain
{
    [DynamoDBTable("temperature-metadata")]
    public class ImportFileDataEntity
    {
        public string Bucket { get; set; }
        [DynamoDBHashKey]
        public string Key { get; set; }
        public long Bytes { get; set; }
        public int Records { get; set; }
        public int BadRecords { get; set; }
        public DateTime ProcessingStarted { get; set; }
        [DynamoDBRangeKey]
        public DateTime FirstRecordTimestamp { get; set; }
        public DateTime LastRecordTimestamp { get; set; }
        public List<string> Dates { get; set; }
    }
}
