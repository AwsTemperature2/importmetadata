using AutoMapper;
using Temperature.ImportMetadata.Domain;

namespace Temperature.ImportMetadata.Infrastructure.Mapping
{
    public class DefaultMappingProfile : Profile
    {
        public DefaultMappingProfile()
        {
            this.CreateMap<ImportFileData, ImportFileDataEntity>();
            
        }
    }
}
