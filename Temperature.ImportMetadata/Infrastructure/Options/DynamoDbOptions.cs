namespace Temperature.ImportMetadata.Infrastructure.Options
{
    public class DynamoDbOptions
    {
        public string Prefix { get; set; }
    }
}
